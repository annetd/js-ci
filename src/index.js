require('babel-polyfill');

const spread = obj => ({
  ...obj,
  foo: 'bar'
});

module.exports = { spread };
