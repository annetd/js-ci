const mod = require('./index');

describe('spread', () => {
  it('should spread object', () => {
    const expected = { message: 'Hello, world!', foo: 'bar' };
    const ret = mod.spread({ message: 'Hello, world!' });

    expect(ret).toEqual(expected);
  });
});
